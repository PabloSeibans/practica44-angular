import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FrontendComponent } from './pages/frontend/frontend.component';
import { BackendComponent } from './pages/backend/backend.component';
import { FullstackComponent } from './pages/fullstack/fullstack.component';
import { EspecialidadesRoutingModule } from './especialidades-routing.module';



@NgModule({
  declarations: [
    FrontendComponent,
    BackendComponent,
    FullstackComponent
  ],
  imports: [
    CommonModule,
    EspecialidadesRoutingModule
  ]
})
export class EspecialidadesModule { }
