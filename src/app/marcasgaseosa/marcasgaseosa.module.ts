import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MarcasgaseosaRoutingModule } from './marcasgaseosa-routing.module';
import { CocacolaComponent } from './pages/cocacola/cocacola.component';
import { FantaComponent } from './pages/fanta/fanta.component';
import { SpriteComponent } from './pages/sprite/sprite.component';


@NgModule({
  declarations: [
    CocacolaComponent,
    FantaComponent,
    SpriteComponent
  ],
  imports: [
    CommonModule,
    MarcasgaseosaRoutingModule
  ]
})
export class MarcasgaseosaModule { }
