import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AsusComponent } from './pages/asus/asus.component';
import { DellComponent } from './pages/dell/dell.component';
import { LenovoComponent } from './pages/lenovo/lenovo.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'lenovo', component: LenovoComponent },
      { path: 'dell', component: DellComponent },
      { path: 'asus', component: AsusComponent },
      { path: '**', redirectTo: 'lenovo' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MarcaslaptopRoutingModule { }
