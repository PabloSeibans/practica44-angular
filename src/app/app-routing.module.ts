import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'especialidades',
    loadChildren: () => import('./especialidades/especialidades.module').then(m => m.EspecialidadesModule)
  },
  {
    path: 'carreras',
    loadChildren: () => import('./carreras/carreras.module').then(m => m.CarrerasModule)
  },
  {
    path: 'marcaslaptop',
    loadChildren: () => import('./marcaslaptop/marcaslaptop.module').then(m => m.MarcaslaptopModule)
  },
  {
    path: 'marcasgaseosa',
    loadChildren: () => import('./marcasgaseosa/marcasgaseosa.module').then(m => m.MarcasgaseosaModule)
  },
  {
    path: '**',
    redirectTo: 'especialidades'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
